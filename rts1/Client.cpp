#include "Client.hpp"
#include "Control.hpp"
#include "Visualizer.hpp"
#include <vector>
#include <SFML/Network.hpp>

Client::Client(Robot &robot, Visualizer &visualizer)
	: robot(robot), visualizer(visualizer)
{
}

Client::~Client()
{
}

void Client::setFoulExitCode(int code)
{
	foulExitCode = code;
}

void Client::setRemoteAddress(std::string address)
{
	remoteAddress = address;
}

void Client::setTimeLimit(int limit)
{
	timelimit = limit;
}

void Client::connect()
{
	sf::TcpSocket socket;
	if (socket.connect(remoteAddress, remotePort) != sf::Socket::Done)
	{
		throw std::runtime_error("Unable to establish connection");
	}
    int task = 1;
    Status status;
    Control control;
	for (bool respond = true; active; respond ^= true) {
		std::vector<byte> statusBuffer(bufferSize);
		size_t bytesReceived;
		if (socket.receive(statusBuffer.data(), bufferSize, bytesReceived) != sf::Socket::Done)
		{
			throw std::runtime_error("Unable to receive packet");
		}
        status = statusBuffer.data();
		if (timelimit && status.timestamp > timelimit) {
			std::cout << "Timelimit exceeded" << std::endl;
			exit(foulExitCode);
		}
		if (foulExitCode) {
			if (status.failed || status.crashed) {
				exit(foulExitCode);
			}
			if (status.done) {
				exit(EXIT_SUCCESS);
			}
		}
		if (task < status.task) {
            std::cout << "Task " << task << " done in "
                      << status.timestamp << " s" << std::endl;
            task = status.task;
        }
        robot.updateStatus(status);
        if (respond) {
            control = robot.updateControl();
            std::string controlBuffer = control.toRaw();
            socket.send(controlBuffer.data(), controlBuffer.size());
        }
        visualizer.update(status, control);
	}
}

void Client::disconnect()
{
	active = false;
}