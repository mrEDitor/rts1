#pragma once
#include "main.hpp"

class Status
{

public:
	Status();
	Status(cstring raw);
	~Status();

	std::string toString() const;

	double timestamp;
	double robotRadius, robotMass, robotX, robotY, robotXSpeed, robotYSpeed, robotFuel;
	double targetRadius, targetX, targetY;
	int task, destinationIndex;
	int failed, crashed, done;
	int errorCount, ignoreCount;
};
