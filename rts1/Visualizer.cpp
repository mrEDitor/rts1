#include "Visualizer.hpp"
#include "Client.hpp"

Visualizer::Visualizer(Robot const& robot)
	: robot(robot)
{
	setFont(FONT_FILE_DEFAULT);
	text.setString("No connection");
	text.setCharacterSize(40);
	text.setPosition(windowSide / 2, windowSide / 2);
}

Visualizer::~Visualizer()
{
}

void Visualizer::setClient(Client *client)
{
	this->client = client;
}

void Visualizer::setFont(std::string fontFileName)
{
	if (!font.loadFromFile(fontFileName))
	{
		throw std::runtime_error("Unable to load font!");
	}
	text.setFont(font);
}

void Visualizer::setWindowSide(int side)
{
	windowSide = side;
	fieldOffset = side / 2;
	fieldScale = side / 4;
}

void Visualizer::setShowWindow(bool show)
{
	showWindow = show;
}

void Visualizer::update(Status const& status, Control const& control)
{
	mutex.lock();
	controlEvent = &control;
	statusEvent = &status;
	text.setString(status.toString() + control.toString());
	text.setCharacterSize(100);
	float scale = windowSide / text.getLocalBounds().height;
	text.setScale(scale, scale);
	text.setPosition(windowSide, 0);
	mutex.unlock();
}

void Visualizer::start()
{
	if (!showWindow) {
		return;
	}

	sf::Uint32 style = sf::Style::Titlebar | sf::Style::Close;
	sf::VideoMode mode(windowSide * 3 / 2, windowSide);
	window.create(mode, WINDOW_TITLE, style);
	window.setVerticalSyncEnabled(true);

	sf::Vertex grid[4] =
	{
		sf::Vertex(sf::Vector2f(windowSide / 2, 0), sf::Color::White),
		sf::Vertex(sf::Vector2f(windowSide / 2, windowSide), sf::Color::White),
		sf::Vertex(sf::Vector2f(0, windowSide / 2), sf::Color::White),
		sf::Vertex(sf::Vector2f(windowSide, windowSide / 2), sf::Color::White),
	};

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			switch (event.type) {
			case sf::Event::Closed:
				client->disconnect();
				window.close();
				break;
			}
		}

		if (!statusEvent && !controlEvent) {
			window.clear();
			window.draw(text);
			window.display();
			sf::sleep(sf::microseconds(100));
			continue;
		}

		double robotX = fieldOffset + statusEvent->robotX * fieldScale;
		double robotY = fieldOffset - statusEvent->robotY * fieldScale;

		sf::CircleShape robot(statusEvent->robotRadius * fieldScale);
		robot.setFillColor(sf::Color::Cyan);
		robot.setOrigin(statusEvent->robotRadius * fieldScale, statusEvent->robotRadius * fieldScale);
		robot.setPosition(robotX, robotY);

		sf::CircleShape target(std::max(statusEvent->targetRadius * fieldScale, robot.getRadius() / 2.0));
		target.setOrigin(target.getRadius(), target.getRadius());
		target.setFillColor(sf::Color::Yellow);
		target.setPosition(
			fieldOffset + this->robot.targetX * fieldScale,
			fieldOffset - this->robot.targetY * fieldScale
		);

		sf::Vertex velocity[2] =
		{
			sf::Vertex(sf::Vector2f(robotX, robotY), sf::Color::Red),
			sf::Vertex(sf::Vector2f(
				robotX + controlEvent->robotXVelocity * fieldScale,
				robotY - controlEvent->robotYVelocity * fieldScale
			), sf::Color::Red),
		};
		sf::Vertex speed[2] =
		{
			sf::Vertex(sf::Vector2f(robotX, robotY), sf::Color::Green),
			sf::Vertex(sf::Vector2f(
				robotX + statusEvent->robotXSpeed * fieldScale,
				robotY - statusEvent->robotYSpeed * fieldScale
			), sf::Color::Green),
		};

		mutex.lock();
		window.clear();
		window.draw(grid, 4, sf::Lines);
		window.draw(target);
		window.draw(robot);
		window.draw(velocity, 2, sf::Lines);
		window.draw(speed, 2, sf::Lines);
		window.draw(text);
		window.display();
		mutex.unlock();
	}
}
