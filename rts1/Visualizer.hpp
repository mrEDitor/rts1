#pragma once
class Client;
#include "Control.hpp"
#include "Robot.hpp"
#include "Status.hpp"
#include <SFML/Graphics.hpp>

#define FONT_FILE_DEFAULT "PTM75F.ttf"
#define WINDOW_TITLE "Mobile robot visualization"

class Visualizer
{

public:
	Visualizer(Robot const& robot);
	~Visualizer();

	void setClient(Client *client);
	void setFont(std::string fontFileName);
	void setShowWindow(bool show);
	void setWindowSide(int side);

	void update(Status const& status, Control const& control);
	void start();

private:

	bool showWindow = true;
	int windowSide = 600;
	int fieldOffset = windowSide / 2;
	double fieldScale = windowSide / 2;
	sf::RenderWindow window;
	sf::Mutex mutex;

	sf::Font font;
	sf::Text text;
	Client *client;
	Robot const& robot;
	Status const* statusEvent;
	Control const* controlEvent;

};
