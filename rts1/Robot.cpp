#include "Robot.hpp"
#include <algorithm>

Robot::Robot()
{
}

Robot::~Robot()
{
}

void Robot::setCoefficients(double proportional, double integral, double differential)
{
	proportionalCoefficient = proportional;
	integralCoefficient = integral;
	differentialCoefficient = differential;
}

void Robot::updateStatus(Status const& status)
{
	this->status = &status;
}

Control Robot::updateControl()
{
	Control control;
	bool stabilizing = sqr(status->robotXSpeed) + sqr(status->robotYSpeed) < 1e-4;
	if (!stable) {
		if (stabilizing) {
			stable = true;
		} else {
			control.robotXVelocity = status->robotXSpeed * 2 / 3;
			control.robotYVelocity = status->robotYSpeed * 2 / 3;
			return control;
		}
	}
	if (stabilizing) {
		switch (status->task) {
		case 1:
			targetX = 0;
			targetY = 0;
			break;
		case 2:
			targetX = targets1X[status->destinationIndex];
			targetY = targets1Y[status->destinationIndex];
			break;
		case 3:
			targetX = status->targetX * (1.0 - 0.5 * std::abs(status->robotX - status->targetX));
			targetY = status->targetY * (1.0 - 0.5 * std::abs(status->robotY - status->targetY));
			break;
		}
	}
	double speedX = status->robotX - targetX;
	double speedY = status->robotY - targetY;
	double errorX = status->robotXSpeed + speedX;
	double errorY = status->robotYSpeed + speedY;
	integralLastX += integralCoefficient * errorX;
	integralLastY += integralCoefficient * errorY;
	double differentialX = differentialCoefficient * (errorX - errorLastX);
	double differentialY = differentialCoefficient * (errorY - errorLastY);
	control.robotXVelocity = proportionalCoefficient * errorX + integralLastX + differentialX;
	control.robotYVelocity = proportionalCoefficient * errorY + integralLastY + differentialY;
	errorLastX = errorX;
	errorLastY = errorY;
	return control;
}
