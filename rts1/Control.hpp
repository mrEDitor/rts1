#pragma once
#include "main.hpp"

class Control
{

public:
	Control();
	~Control();

	std::string toString() const;
	std::string toRaw() const;

	double robotXVelocity, robotYVelocity;
};

