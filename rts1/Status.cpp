#include "Status.hpp"
#include <iostream>
#include <sstream>

Status::Status()
{
}

Status::Status(cstring raw)
{
	cstring offset = raw;
	offset = strscan(offset, "{");
	offset = strscan(offset, "T", timestamp);
	offset = strscan(offset, "R", robotRadius);
	offset = strscan(offset, "M", robotMass);
	offset = strscan(offset, "RX", robotX);
	offset = strscan(offset, "RY", robotY);
	offset = strscan(offset, "VRX", robotXSpeed);
	offset = strscan(offset, "VRY", robotYSpeed);
	offset = strscan(offset, "TR", targetRadius);
	offset = strscan(offset, "TX", targetX);
	offset = strscan(offset, "TY", targetY);
	offset = strscan(offset, "TASK", task);
	offset = strscan(offset, "POINT", destinationIndex);
	offset = strscan(offset, "FAIL", failed);
	offset = strscan(offset, "CHASH", crashed);
	offset = strscan(offset, "FUEL", robotFuel);
	offset = strscan(offset, "DONE", done);
	offset = strscan(offset, "ERROR", errorCount);
	offset = strscan(offset, "IGNORED", ignoreCount);
	offset = strscan(offset, "}");
	size_t len = strlen(offset);
	memmove(raw, offset, len);
	raw[len] = NULL;
}

Status::~Status()
{
}

std::string Status::toString() const
{
	std::stringstream s;
	s << "Current status" << std::endl;
	s << "T = " << timestamp << std::endl;
	s << "R = " << robotRadius << std::endl;
	s << "M = " << robotMass << std::endl;
	s << "RX = " << robotX << std::endl;
	s << "RY = " << robotY << std::endl;
	s << "VRX = " << robotXSpeed << std::endl;
	s << "VRY = " << robotYSpeed << std::endl;
	s << "TR = " << targetRadius << std::endl;
	s << "TX = " << targetX << std::endl;
	s << "TY = " << targetY << std::endl;
	s << "TASK = " << task << std::endl;
	s << "POINT = " << destinationIndex << std::endl;
	s << "FAIL = " << failed << std::endl;
	s << "CRASH = " << crashed << std::endl;
	s << "FUEL = " << robotFuel << std::endl;
	s << "DONE = " << done << std::endl;
	s << "ERROR = " << errorCount << std::endl;
	s << "IGNORED = " << ignoreCount << std::endl;
	s << std::endl;
	return s.str();
}
