#include "main.hpp"
#include "Client.hpp"
#include "Robot.hpp"
#include "Visualizer.hpp"
#include <algorithm>

void help(cstring path) {
	std::cout
		<< "Mobile robot control and visualisation system" << std::endl
		<< "Usage:" << std::endl
		<< TAB << path << "[--coeff Kp Ki Kd] [--server ADDRESS] [--window HEIGHT | --no-window]"
		<< " [--time-limit SECONDS] [--foul-exit-code CODE] [--font FILE]" << std::endl
		;
	exit(EXIT_SUCCESS);
}

int main(int argc, cstring argv[])
{
	Robot robot;
	Visualizer visualizer(robot);
	Client client(robot, visualizer);

	for (int i = 1; i < argc; ++i) {
		cstring arg = argv[i];
		if (streq(arg, "--server")) {
			client.setRemoteAddress(argv[++i]);
		}
		else if (streq(arg, "--foul-exit-code")) {
			client.setFoulExitCode(std::atoi(argv[++i]));
		}
		else if (streq(arg, "--time-limit")) {
			client.setTimeLimit(std::atoi(argv[++i]));
		}
		else if (streq(arg, "--no-window")) {
			visualizer.setShowWindow(false);
		}
		else if (streq(arg, "--window")) {
			visualizer.setWindowSide(std::atoi(argv[++i]));
		}
		else if (streq(arg, "--font")) {
			visualizer.setFont(argv[++i]);
		}
		else if (streq(arg, "--coeff")) {
			robot.setCoefficients(std::stod(argv[i + 1]), std::stod(argv[i + 2]), std::stod(argv[i + 3]));
			i += 3;
		}
		else {
			help(argv[0]);
		}
	}

	sf::Thread visulization(&Visualizer::start, &visualizer);
	try {
		visualizer.setClient(&client);
		visulization.launch();
		client.connect();
	}
	catch (std::exception const& exception) {
		std::cerr << exception.what() << std::endl;
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}
