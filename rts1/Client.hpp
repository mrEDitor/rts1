#pragma once
#include "main.hpp"
#include "Robot.hpp"
#include "Visualizer.hpp"

class Client
{

public:
	Client(Robot &robot, Visualizer &visualizer);
	~Client();

	void setFoulExitCode(int code);
	void setRemoteAddress(std::string address);
	void setTimeLimit(int limit);

	void connect();
	void disconnect();

private:
	bool active = true;
	size_t bufferSize = 2048;
	int foulExitCode = 0;
	short remotePort = 9999;
	int timelimit = 0;
	std::string remoteAddress = "127.0.0.1";
	Robot &robot;
	Visualizer &visualizer;

};
