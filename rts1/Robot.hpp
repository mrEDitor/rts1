#pragma once
#include "main.hpp"
#include "Control.hpp"
#include "Status.hpp"

class Robot
{

public:
	Robot();
	~Robot();
	
	void setCoefficients(double proportional, double integral, double differential);
	void updateStatus(Status const& status);
	Control updateControl();

	double targetX = 0;
	double targetY = 0;

private:
	double targets1X[6] = { +0.7, +0.0, -0.3, -0.7, +0.7, +0.0 };
	double targets1Y[6] = { +0.0, +0.7, +0.0, +0.0, +0.0, -0.8 };
	
	Status const* status;
	double proportionalCoefficient = 2;
	double integralCoefficient = 0;
	double differentialCoefficient = 35;

	bool stable = false;
	double integralLastX = 0;
	double integralLastY = 0;
	double errorLastX = 0;
	double errorLastY = 0;

};
