#pragma once

#include <cstring>
#include <iostream>
#include <stdexcept>
#include <string>
#define TAB "\t"
#define sqr(x) ((x)*(x))
#define range(x) (x).begin(), (x).end()

typedef char byte;
typedef char* cstring;

inline bool streq(const cstring a, const cstring b)
{
	return strcmp(a, b) == 0;
}

inline void throwCommunicationException(cstring str, cstring message)
{
	std::cerr << "> " << str << std::endl;
	throw std::runtime_error(message);
}

inline cstring strscan(cstring str, cstring val)
{
	if (strncmp(val, str, strlen(val)) != 0) {
		throwCommunicationException(str, "Unable to parse received input!");
	}
	return str + strlen(val);
}

inline cstring strscan(cstring str, cstring name, double &val)
{
	int n;
	char read[16];
	if (sscanf(str, "(%[A-Z]=%lf)%n", &read, &val, &n) < 2) {
		throwCommunicationException(str, "Unable to parse received input!");
	}
	if (!streq(name, read)) {
		throwCommunicationException(str, "Unexpected input parameter!");
	}
	return str + n;
}

inline cstring strscan(cstring str, cstring name, int &val)
{
	int n;
	char read[16];
	if (sscanf(str, "(%[A-Z]=%d)%n", &read, &val, &n) < 2) {
		std::cerr << "> " << str << std::endl;
		throwCommunicationException(str, "Unable to parse received input!");
	}
	if (!streq(name, read)) {
		throwCommunicationException(str, "Unexpected input parameter!");
	}
	return str + n;
}

int main(int argc, cstring argv[]);
