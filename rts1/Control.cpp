#include "Control.hpp"
#include <iomanip>
#include <sstream>

Control::Control()
	: robotXVelocity(0), robotYVelocity(0)
{
}

Control::~Control()
{
}

std::string Control::toString() const
{
	std::stringstream s;
	s << "Current control" << std::endl;
	s << "FRX = " << std::setprecision(8) << robotXVelocity << std::endl;
	s << "FRY = " << std::setprecision(8) << robotYVelocity << std::endl;
	return s.str();
}

std::string Control::toRaw() const
{
	std::stringstream s;
	s << "{";
	s << "(FRX=" << robotXVelocity << ")";
	s << "(FRY=" << robotYVelocity << ")";
	s << "}";
	return s.str();
}
